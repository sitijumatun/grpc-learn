package repository

import (
	"context"
	"grpc_hello_world/domain/entity"
)

type UserRepository interface {
	GetAll(ctx context.Context, q string) ([]*entity.User, error)
	Create(ctx context.Context, user *entity.User) error
	Get(ctx context.Context, id int32) (*entity.User, error)
	Update(ctx context.Context, user *entity.User, id int32) error
	Delete(ctx context.Context, id int32) error
}
