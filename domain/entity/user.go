package entity

type User struct {
	ID      int32
	Name    string
	Address string
}
