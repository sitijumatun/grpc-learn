-- +goose Up
ALTER TABLE users ADD COLUMN phone text NOT NULL DEFAULT '';

-- +goose Down
ALTER TABLE users DROP COLUMN phone;
