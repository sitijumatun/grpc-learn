package main

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestDocSetup_AddRecipient(t *testing.T) {
	history := NewHistory()

	document := &Document{
		ID:          "uid1",
		Title:       "doc1",
		Description: "desc1",
		Recipients:  nil,
	}

	docData := New(document)

	recipient1 := &Recipient{
		ID:           "uidrecp1",
		DocumentID:   document.ID,
		Role:         "SIGNER",
		Order:        1,
		Placeholders: nil,
	}

	docData.AddRecipient(recipient1)
	history.Save(docData.Memento())

	recipient2 := &Recipient{
		ID:           "uidrecp2",
		DocumentID:   document.ID,
		Role:         "APPROVER",
		Order:        2,
		Placeholders: nil,
	}

	docData.AddRecipient(recipient2)
	history.Save(docData.Memento())

	recipient3 := &Recipient{
		ID:           "uidrecp3",
		DocumentID:   document.ID,
		Role:         "APPROVER",
		Order:        3,
		Placeholders: nil,
	}

	docData.AddRecipient(recipient3)
	history.Save(docData.Memento())

	recipient4 := &Recipient{
		ID:           "uidrecp4",
		DocumentID:   document.ID,
		Role:         "APPROVER",
		Order:        4,
		Placeholders: nil,
	}

	docData.AddRecipient(recipient4)
	history.Save(docData.Memento())

	t.Run("Summing Count Of Data", func(t *testing.T) {
		assert.Equal(t, 4, len(docData.GetRecipient()))
	})

	t.Run("try to undo once", func(t *testing.T) {
		docData.Restore(history.UndoAssignRecipient())
		assert.Equal(t, 3, len(docData.GetRecipient()))
	})

	t.Run("try to undo twice", func(t *testing.T) {
		docData.Restore(history.UndoAssignRecipient())
		assert.Equal(t, 2, len(docData.GetRecipient()))
	})

	t.Run("try to undo third", func(t *testing.T) {
		docData.Restore(history.UndoAssignRecipient())
		assert.Equal(t, 1, len(docData.GetRecipient()))
	})

	t.Run("try to redo once", func(t *testing.T) {
		docData.Restore(history.RedoAssignRecipient())
		assert.Equal(t, 2, len(docData.GetRecipient()))
	})

	t.Run("try to redo once", func(t *testing.T) {
		docData.Restore(history.UndoAssignRecipient())
		assert.Equal(t, 1, len(docData.GetRecipient()))
	})

	recipient5 := &Recipient{
		ID:           "uidrecp5",
		DocumentID:   document.ID,
		Role:         "APPROVER",
		Order:        5,
		Placeholders: nil,
	}

	docData.AddRecipient(recipient5)
	history.Save(docData.Memento())

	t.Run("Summing Count Of Data After Assign Again", func(t *testing.T) {
		assert.Equal(t, 2, len(docData.GetRecipient()))
	})

	t.Run("try to redo once", func(t *testing.T) {
		docData.Restore(history.UndoAssignRecipient())
		assert.Equal(t, 1, len(docData.GetRecipient()))
	})
}
