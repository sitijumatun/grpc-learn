package main

type Memento struct {
	recipients Recipients
}

func (m *Memento) Recipients() Recipients {
	return m.recipients
}
