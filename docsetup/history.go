package main

type History struct {
	history      []Memento
	currentIndex int
}

func NewHistory() *History {
	return &History{make([]Memento, 0), -1}
}

func (h *History) Save(m Memento) {
	h.history = append(h.history, m)
	h.currentIndex = len(h.history) - 1
}

func (h *History) UndoAssignRecipient() Memento {
	if len(h.history) > 1 && h.currentIndex > 0 {
		h.currentIndex--
		return h.history[h.currentIndex]
	} else {
		return Memento{}
	}
}

func (h *History) RedoAssignRecipient() Memento {
	if h.currentIndex < len(h.history)-1 {
		h.currentIndex++
		result := h.history[h.currentIndex]

		return result
	} else {
		return Memento{}
	}
}

//func (h *History) Undo() Memento {
//	if h.currentIndex > len(h.history)-1 {
//		h.currentIndex--
//		historyUpdatedUndo := h.history[:h.currentIndex]
//		return h.history[len(historyUpdatedUndo)-1]
//		//n := len(h.history) - 1
//		//h.history = h.history[:n]
//		//return h.history[len(h.history)-1]
//
//	} else {
//		return Memento{}
//	}
//}

//func (h *History) Undo() Memento {
//	if len(h.history) > 1 {
//		n := len(h.history) - 1
//		h.Currenthistory = h.history[:n]
//		return h.Currenthistory[len(h.Currenthistory)-1]
//	} else {
//		return Memento{}
//	}
//}

//func (h *History) Redo() Memento {
//	if h.currentIndex < len(h.history)-1 {
//		h.currentIndex++
//		historyUpdatedRedo := h.history[:h.currentIndex]
//		return h.history[len(historyUpdatedRedo)+1]
//		//n := len(h.history) - 1
//		//h.history = h.history[:n]
//		//return h.history[len(h.history)-1]
//
//	} else {
//		return Memento{}
//	}
//}
