package main

import "fmt"

func main() {
	//doc := &Document{
	//	ID:          "uid1",
	//	Title:       "jeje1",
	//	Description: "ini dokumen coba coba",
	//	Recipients:  nil,
	//}
	//
	//recipent1 := &Recipient{
	//	ID:         "docrecp1",
	//	DocumentID: doc.ID,
	//	Role:       "SIGNER",
	//	Order:      1,
	//	Placeholders: Placeholders{
	//		&Placeholder{
	//			PosX:       400,
	//			PosY:       900,
	//			PageNumber: 1,
	//		},
	//	},
	//}
	//
	//doc.Recipients = Recipients{
	//	recipent1,
	//}

	history := NewHistory()

	document := &Document{
		ID:          "uid1",
		Title:       "doc1",
		Description: "desc1",
		Recipients:  nil,
	}

	docData := New(document)

	recipient1 := &Recipient{
		ID:           "uidrecp1",
		DocumentID:   document.ID,
		Role:         "SIGNER",
		Order:        1,
		Placeholders: nil,
	}

	docData.AddRecipient(recipient1)
	history.Save(docData.Memento())

	recipient2 := &Recipient{
		ID:           "uidrecp2",
		DocumentID:   document.ID,
		Role:         "APPROVER",
		Order:        2,
		Placeholders: nil,
	}

	docData.AddRecipient(recipient2)
	history.Save(docData.Memento())

	recipient3 := &Recipient{
		ID:           "uidrecp3",
		DocumentID:   document.ID,
		Role:         "APPROVER",
		Order:        3,
		Placeholders: nil,
	}
	docData.AddRecipient(recipient3)
	history.Save(docData.Memento())

	recipient4 := &Recipient{
		ID:           "uidrecp4",
		DocumentID:   document.ID,
		Role:         "APPROVER",
		Order:        4,
		Placeholders: nil,
	}
	docData.AddRecipient(recipient4)
	history.Save(docData.Memento())

	fmt.Printf("JUMLAH RECIPIENT DATA SEBELUM UNDO : %v\n", len(docData.GetRecipient()))

	docData.Restore(history.UndoAssignRecipient())
	fmt.Printf("JUMLAH RECIPIENT DATA UNDO KE - 1 : %v\n", len(docData.GetRecipient()))

	docData.Restore(history.UndoAssignRecipient())
	fmt.Printf("JUMLAH RECIPIENT DATA UNDO KE - 2 : %v\n", len(docData.GetRecipient()))

	docData.Restore(history.RedoAssignRecipient())
	fmt.Printf("JUMLAH RECIPIENT DATA REDO KE - 1 : %v\n", len(docData.GetRecipient()))

	//fmt.Println(len(docData.GetRecipient()))
	//
	//docData.Restore(history.Undo())
	//fmt.Println(len(docData.GetRecipient()))
	//
	//docData.Restore(history.Undo())
	//fmt.Println(len(docData.GetRecipient()))
	//
	//docData.Restore(history.Undo())
	//fmt.Println(len(docData.GetRecipient()))

	//fmt.Println("redo data")
	//
	//docData.Restore(history.Redo())
	//fmt.Println(len(docData.GetRecipient()))
	//
	//docData.Restore(history.Redo())
	//fmt.Println(len(docData.GetRecipient()))

	//docData.Restore(history.Redo())
	//fmt.Println(len(docData.GetRecipient()))

	//docData.Restore(history.Undo())
	//fmt.Println(len(docData.GetRecipient()))

}
