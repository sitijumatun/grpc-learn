package main

import "fmt"

func main() {
	objectToStore := &objectToStore{
		mementoArray: make([]*MementoState, 0),
	}

	mementoStore := &MementoStore{
		state: "StoreMe 1",
	}

	fmt.Printf("Originator Current State: %sn", mementoStore.getState())
	objectToStore.addMemento(mementoStore.createMemento())

	mementoStore.setState("StoreMe 2")
	fmt.Printf("Originator Current State: %sn", mementoStore.getState())

	objectToStore.addMemento(mementoStore.createMemento())
	mementoStore.setState("StoreMe 3")

	fmt.Printf("Originator Current State: %sn", mementoStore.getState())

	objectToStore.addMemento(mementoStore.createMemento())

	mementoStore.restoreMemento(objectToStore.getMemento(1))
	fmt.Printf("Restored to State: %sn", mementoStore.getState())

	mementoStore.restoreMemento(objectToStore.getMemento(0))
	fmt.Printf("Restored to State: %sn", mementoStore.getState())
}
