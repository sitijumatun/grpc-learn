package main

type MementoStore struct {
	state string
}

func (e *MementoStore) createMemento() *MementoState {
	return &MementoState{state: e.state}
}

func (e *MementoStore) restoreMemento(m *MementoState) {
	e.state = m.getSavedState()
}

func (e *MementoStore) setState(state string) {
	e.state = state
}

func (e *MementoStore) getState() string {
	return e.state
}

type MementoState struct {
	state string
}

func (m *MementoState) getSavedState() string {
	return m.state
}

type objectToStore struct {
	mementoArray []*MementoState
}

func (c *objectToStore) addMemento(m *MementoState) {
	c.mementoArray = append(c.mementoArray, m)
}

func (c *objectToStore) getMemento(index int) *MementoState {
	return c.mementoArray[index]
}
