package setupdoc

type Memento struct {
	docSetup *DocSetup
}

func (m *Memento) DocSetup() *DocSetup {
	return m.docSetup
}
