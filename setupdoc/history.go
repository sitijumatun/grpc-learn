package setupdoc

type History struct {
	history      []Memento
	currentIndex int
}

func NewHistory() *History {
	return &History{make([]Memento, 0), 0}
}

func (h *History) Save(m Memento) {
	h.history = append(h.history, m)
	h.currentIndex++
}

func (h *History) Undo() Memento {
	if len(h.history) > 1 {
		n := len(h.history) - 1
		h.history = h.history[:n]
		return h.history[len(h.history)-1]
	} else {
		return Memento{}
	}
}

func (h *History) Redo() Memento {
	if len(h.history) < 1 {
		n := len(h.history) + 1
		h.history = h.history[:n]
		return h.history[len(h.history)+1]
	} else {
		return Memento{}
	}
}
