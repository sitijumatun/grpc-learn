package setupdoc

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestSetupDoc(t *testing.T) {
	history := NewHistory()

	document := &Document{
		ID:    "uid1",
		Title: "doc1",
	}
	docData := New(document)
	history.Save(docData.Memento())

	t.Run("Test Doc Created", func(t *testing.T) {
		assert.Equal(t, 0, len(docData.GetRecipient()))
		assert.Equal(t, 1, len(history.history))
	})

	docData.SetDescription("this is doc description")
	history.Save(docData.Memento())

	t.Run("Test Doc Set Description", func(t *testing.T) {
		assert.Equal(t, "this is doc description", docData.Doc.Description)
		docData.Restore(history.Undo())
		assert.Equal(t, nil, docData.Doc.Description)
	})
}
