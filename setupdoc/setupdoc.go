package setupdoc

type Document struct {
	ID          string
	Title       string
	Description string `json:"description" example:"Need your sign, pleas :)"`

	Recipients Recipients
}

type Documents []*Document
type Recipients []*Recipient

type Recipient struct {
	ID         string `json:"id,omitempty"`
	DocumentID string `json:"document-id,omitempty"`
	Role       string `json:"role,omitempty" example:"SIGNER,REVIEWER,APPROVER"`
	Order      int

	Placeholders Placeholders
}

type Placeholders []*Placeholder

type Placeholder struct {
	PosX       float64
	PosY       float64
	PageNumber int
}

type DocSetup struct {
	Doc *Document
}

type State interface{}

func New(d *Document) *DocSetup {
	return &DocSetup{Doc: d}
}

func (d *DocSetup) AddRecipient(r *Recipient) {
	if d.Doc.Recipients == nil {
		d.Doc.Recipients = Recipients{}
	}

	d.Doc.Recipients = append(d.Doc.Recipients, r)
}

func (d *DocSetup) SetDescription(desc string) {
	d.Doc.Description = desc
}

func (d *DocSetup) GetRecipient() []*Recipient {
	return d.Doc.Recipients
}

func (d *DocSetup) GetDocument() *Document {
	return d.Doc
}

func (d *DocSetup) Memento() Memento {
	return Memento{docSetup: &DocSetup{}}
}

func (d *DocSetup) Restore(m Memento) {
	d = m.docSetup
}
