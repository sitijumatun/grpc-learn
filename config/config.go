package config

import "os"

type DBConfig struct {
	DBDriver   string
	DBHost     string
	DBPort     string
	DBUser     string
	DBName     string
	DBPassword string
	DBTimeZone string
	DBLog      bool
}

type Config struct {
	DBConfig
	TestDBConfig DBConfig
}

func New() *Config {
	return &Config{
		DBConfig: DBConfig{
			DBDriver:   getEnv("DB_DRIVER", "mysql"),
			DBHost:     getEnv("DB_HOST", "localhost"),
			DBPort:     getEnv("DB_PORT", "3306"),
			DBUser:     getEnv("DB_USER", "root"),
			DBName:     getEnv("DB_NAME", "go_rest_skeleton"),
			DBPassword: getEnv("DB_PASSWORD", ""),
			DBTimeZone: getEnv("APP_TIMEZONE", "Asia/Jakarta"),
		},
		TestDBConfig: DBConfig{
			DBDriver:   getEnv("TEST_DB_DRIVER", "mysql"),
			DBHost:     getEnv("TEST_DB_HOST", "localhost"),
			DBPort:     getEnv("TEST_DB_PORT", "3306"),
			DBUser:     getEnv("TEST_DB_USER", "root"),
			DBName:     getEnv("TEST_DB_NAME", "go_rest_skeleton_test"),
			DBPassword: getEnv("TEST_DB_PASSWORD", ""),
			DBTimeZone: getEnv("APP_TIMEZONE", "Asia/Jakarta"),
		},
	}
}

func getEnv(key string, defaultVal string) string {
	//if value, exists := os.LookupEnv(key); exists {
	//	return value
	//}

	value, exists := os.LookupEnv(key)
	if exists {
		return value
	}

	//if nextValue := os.Getenv(key); nextValue != "" {
	//	return nextValue
	//}

	nextValue := os.Getenv(key)

	if nextValue != "" {
		return nextValue
	}

	return defaultVal
}
