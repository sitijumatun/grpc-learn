package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

func messagePrint(total int, message string) {
	for i := 0; i < total; i++ {
		fmt.Printf("%d: %s\n", i, message)
		amt := time.Duration(rand.Intn(250))
		time.Sleep(time.Millisecond * amt)
	}
}

func receiver(channel chan string, spv *sync.WaitGroup) {
	defer spv.Done()
	message := <-channel
	fmt.Printf("Menerima suatu pesan dari channel : %s\n", message)
}

func sender(channel chan string, message string) {
	channel <- message
}

func main() {
	//     var a string

	spv := &sync.WaitGroup{}

	// Unbuffered channel
	channelA := make(chan string)

	spv.Add(2)

	go receiver(channelA, spv)
	go receiver(channelA, spv)

	sender(channelA, "halo")
	sender(channelA, "halo kedua")

	// Blocking
	spv.Wait()

	// 	time.Sleep(2 * time.Second)

	// Buffered channel
	// 	channelB := make(chan int, 10)

	// Channel itu sifatnya blocking
	// Channel itu syaratnya harus ada pengirim dan penerima
	// 1 pengirim harus diterima 1 penerima

	// 	go messagePrint(10, "hello world")
	// 	go messagePrint(10, "welcome")
	// 	go messagePrint(5, "carstensz")
	//
	// 	time.Sleep(5 * time.Second)
}
