package interceptor

import (
	"context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
	"log"
)

func isValidAppKey(appKey []string) bool {
	if len(appKey) < 1 {
		return false
	}

	return appKey[0] == "210397"
}

func UnaryInterceptorImpl(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (resp interface{}, err error) {
	//fmt.Printf("Incoming request: %s\n" + info.FullMethod)
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return nil, status.Errorf(codes.InvalidArgument, "Invalid Argument")
	}

	if !isValidAppKey(md["app_key"]) {
		return nil, status.Errorf(codes.Unauthenticated, "Unauthenticated")
	}

	m, err := handler(ctx, req)
	if err != nil {
		log.Fatalf("error in grpc service %v", err)
	}

	return m, err
}
