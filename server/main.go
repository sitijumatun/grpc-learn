package main

import (
	"fmt"
	"github.com/joho/godotenv"
	"github.com/pressly/goose/v3"
	"github.com/urfave/cli/v2"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"grpc_hello_world/config"
	"grpc_hello_world/domain/entity"
	"grpc_hello_world/infrastructure/dao"
	"grpc_hello_world/interceptor"
	"grpc_hello_world/interfaces/workflow"
	"grpc_hello_world/model/user"
	"log"
	"net"
	"os"
)

func main() {
	// load ENV
	//if err := godotenv.Load(); err != nil {
	//	log.Println("no .env file provided")
	//}

	err := godotenv.Load()
	if err != nil {
		log.Println("no .env file provided")
	}

	// Init Config
	conf := config.New()
	dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s sslmode=disable TimeZone=Asia/Jakarta",
		conf.DBHost,
		conf.DBUser,
		conf.DBPassword,
		conf.DBName,
		conf.DBPort,
	)

	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	db.AutoMigrate(&entity.User{})

	app := &cli.App{
		Name:  "boom",
		Usage: "make an explosive entrance",
		Action: func(*cli.Context) error {
			//Create TCP Server on localhost:5001
			lis, err := net.Listen("tcp", ":5002")
			if err != nil {
				log.Fatalf("Failed to listen on port: %v", err)
			}
			//Create new gRPC server handler
			server := grpc.NewServer(grpc.ChainUnaryInterceptor(interceptor.UnaryInterceptorImpl))

			// init repositories
			repo := dao.NewRepositories(db)

			//register gRPC UserService to gRPC server handler
			user.RegisterUserServiceServer(server, workflow.NewUserService(repo))

			reflection.Register(server)

			//Run server
			if err := server.Serve(lis); err != nil {
				return err
			}

			return nil
		},
	}

	commands := []*cli.Command{
		{
			Name:    "db:migrate",
			Aliases: []string{"a"},
			Usage:   "add a task to the list",
			Action: func(cCtx *cli.Context) error {
				if err := goose.SetDialect("postgres"); err != nil {
					return err
				}

				currentDB, err := db.DB()
				if err != nil {
					return err
				}

				if err := goose.Up(currentDB, "migrations/migrations"); err != nil {
					return err
				}

				return nil
			},
		},
		{
			Name:    "db:down",
			Aliases: []string{"a"},
			Usage:   "add a task to the list",
			Action: func(cCtx *cli.Context) error {
				if err := goose.SetDialect("postgres"); err != nil {
					return err
				}

				currentDB, err := db.DB()
				if err != nil {
					return err
				}

				if err := goose.Down(currentDB, "migrations/migrations"); err != nil {
					return err
				}

				return nil
			},
		},
	}

	app.Commands = commands

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}
