package main

type Memento struct {
	list        List
	currentList CurrentList
}

func (m *Memento) List() List {
	return m.list
}

func (m *Memento) CurrentList() CurrentList {
	return m.currentList
}
