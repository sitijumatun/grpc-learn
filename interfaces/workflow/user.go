package workflow

import (
	"context"
	"fmt"
	"grpc_hello_world/domain/entity"
	"grpc_hello_world/infrastructure/dao"
	"grpc_hello_world/model/user"
	"net/http"
	"strconv"
)

type UserService struct {
	user.UnimplementedUserServiceServer
	repo *dao.Repositories
}

func NewUserService(repo *dao.Repositories) *UserService {
	return &UserService{repo: repo}
}

func (*UserService) GreetUser(_ context.Context, req *user.GreetingRequest) (*user.GreetingResponse, error) {
	salutationMessage := fmt.Sprintf("Howdy, %s %s, nice to see you in the future!",
		req.Salutation, req.Name)

	return &user.GreetingResponse{GreetingMessage: salutationMessage}, nil
}

func (*UserService) AllUser(_ context.Context, _ *user.AllUserRequest) (*user.AllUserResponse, error) {
	users := []*user.UserDetail{
		{
			Name:    "Jeje",
			Address: "Jogja",
			Id:      1,
		},
		{
			Name:    "Jay",
			Address: "Madura",
			Id:      2,
		},
	}

	response := &user.AllUserResponse{Users: users}

	return response, nil
}

func (us *UserService) GetUserById(ctx context.Context, req *user.GetUserByIDRequest) (*user.UserDetail, error) {
	userData, err := us.repo.UserRepo.Get(ctx, req.Id)

	if err != nil {
		return nil, err
	}

	response := &user.UserDetail{Name: userData.Name, Address: userData.Address, Id: userData.ID}

	return response, nil
}

func (us *UserService) GetAllUser(ctx context.Context, req *user.GetUserRequest) (*user.AllUserResponse, error) {
	users, err := us.repo.UserRepo.GetAll(ctx, req.Q)
	if err != nil {
		return nil, err
	}

	AllUser := []*user.UserDetail{}

	for _, usr := range users {
		AllUser = append(AllUser, &user.UserDetail{
			Name:    usr.Name,
			Address: usr.Address,
			Id:      usr.ID,
		})
	}

	allUser := &user.AllUserResponse{Users: AllUser}

	return allUser, nil
}

func (us *UserService) CreateUser(ctx context.Context, req *user.CreateUserRequest) (*user.CreateUserResponse, error) {
	//us.DB.Create(&entity.User{ID: req.Id, Name: req.Name, Address: req.Address})
	userDataRequest := entity.User{
		ID:      req.Id,
		Name:    req.Name,
		Address: req.Address,
	}

	err := us.repo.UserRepo.Create(ctx, &userDataRequest)
	if err != nil {
		return nil, err
	}

	message := "user created successfully"
	httpStatus := strconv.Itoa(http.StatusCreated)
	response := &user.CreateUserResponse{HttpStatus: httpStatus, Message: message}

	return response, nil
}

func (us *UserService) CreateUserWithObjectReturn(ctx context.Context, req *user.CreateUserRequest) (*user.UserDetail, error) {
	//err := validation.Validate(req.Id,
	//	validation.Required,
	//	UniqUserID{db: us.DB},
	//)
	//
	//if err != nil {
	//	st := status.New(codes.InvalidArgument, "ada error")
	//	//desc := err.Error()
	//
	//	v := &errdetails.BadRequest_FieldViolation{
	//		Field:       "id",
	//		Description: "iki eror",
	//	}
	//	br := &errdetails.BadRequest{}
	//	br.FieldViolations = append(br.FieldViolations, v)
	//	st, _ = st.WithDetails(br)
	//
	//	return nil, st.Err()
	//}
	//
	//createDataProcess := &entity.User{ID: req.Id, Name: req.Name, Address: req.Address}
	//err = us.DB.Create(createDataProcess).Error
	//
	//if err != nil {
	//	return nil, err
	//}
	//
	//userResult := &user.UserDetail{
	//	Name:    createDataProcess.Name,
	//	Address: createDataProcess.Address,
	//	Id:      createDataProcess.ID,
	//}

	return nil, nil
}

func (us *UserService) UpdateUser(ctx context.Context, req *user.CreateUserRequest) (*user.CreateUserResponse, error) {
	err := us.repo.UserRepo.Update(ctx, &entity.User{
		Name:    req.Name,
		Address: req.Address,
	}, int32(req.Id))

	if err != nil {
		return nil, err
	}

	message := "user updated successfully"
	httpStatus := string(rune(http.StatusCreated))
	response := &user.CreateUserResponse{HttpStatus: httpStatus, Message: message}

	return response, nil

}

func (us *UserService) DeleteUser(ctx context.Context, req *user.DeleteUserRequest) (*user.DeleteUserResponse, error) {
	err := us.repo.UserRepo.Delete(ctx, req.Id)
	if err != nil {
		return nil, err
	}

	return &user.DeleteUserResponse{Message: "Sucessfully"}, nil
}

//type UniqUserID struct {
//	db *gorm.DB
//}
//
//func (u UniqUserID) Validate(value interface{}) error {
//	var total int64
//	u.db.Table("users").Where("id = ?", value).Count(&total)
//
//	if total > 0 {
//		return errors.New("id sudah ada")
//	}
//
//	return nil
//}
