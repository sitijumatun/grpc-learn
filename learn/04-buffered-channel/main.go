package main

import (
	"fmt"
	"time"
)

func main() {
	// Buffered channel
	courier := make(chan string, 5)

	go func() {
		for msg := range courier {
			fmt.Printf("Telah diterima pesan: %s\n", msg)
		}
	}()

	courier <- "Kebaya merah"
	courier <- "Kebaya kuning"
	courier <- "Kebaya hijau"
	courier <- "Kebaya biru"
	courier <- "Kebaya coklat"
	courier <- "Kebaya pink"
	courier <- "Kebaya hitam"
	courier <- "Kebaya putih"

	time.Sleep(5 * time.Second)
}
