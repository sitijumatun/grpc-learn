package main

import (
	"fmt"
	"time"
)

func istirahat() {
	// Defer akan selalu dipanggil terakhir didalam sebuah function
	defer selesai()
	defer cetak("kebaya biru")

	fmt.Println("Bersiap beristirahat...")

	defer cetak("kebaya kuning")

	time.Sleep(5 * time.Second)
}

func selesai() {
	fmt.Println("Aplikasi selesai!")
}

func cetak(msg string) {
	fmt.Println(msg)
}

func main() {
	fmt.Println("Kita mencoba penggunaan defer di Golang")

	istirahat()
}
