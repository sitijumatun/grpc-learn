package main

import (
	"context"
	"fmt"
	"sync"
	"time"
)

type Worker struct {
	ID int
}

type Job struct {
	ID   int
	Task Task
}

type Task func(ctx context.Context) error

type Pool chan Worker
type Queue chan Job

func (pool Pool) Consume(ctx context.Context, queue Queue, errs chan error, cancel context.CancelFunc, wg *sync.WaitGroup) {
	defer wg.Done()

	for job := range queue {
		// Mengambil worker dari pool
		worker := <-pool

		fmt.Printf("Worker #%d sedang menjalankan job #%d\n", worker.ID, job.ID)

		// TODO error handling
		err := job.Task(ctx)
		if err != nil {
			select {
			case errs <- err:
			default:
			}
		}

		fmt.Printf("Worker #%d selesai dengan job #%d\n", worker.ID, job.ID)

		// Tugas telah selesai, maka worker kembali ke pool
		pool <- worker
	}
}

func main() {
	// Membuat pool
	pool := make(Pool, 2)

	// Menginisiasi worker ke pool
	for i := 0; i < cap(pool); i++ {
		pool <- Worker{ID: i}
	}

	var wg sync.WaitGroup

	// Membuat ruang antrian (unbuffered channel)
	queue := make(Queue)
	errs := make(chan error, 1)

	// 	var wadahError []error

	ctx := context.Background()
	ctx, cancel := context.WithCancel(ctx)

	defer cancel() // Make sure it's called to release resources even if no errors

	for i := 0; i < cap(pool); i++ {
		wg.Add(1)

		// Worker siap mulai bekerja
		go pool.Consume(ctx, queue, errs, cancel, &wg)
	}

	//     for err := range errs {
	//         wadahError = append(wadahError, err)
	//     }

	// Mengirim tugas ke pool
	queue <- Job{ID: 0, Task: taskOne}
	queue <- Job{ID: 2, Task: taskErr}
	queue <- Job{ID: 2, Task: taskErr}
	queue <- Job{ID: 2, Task: taskErr}
	queue <- Job{ID: 2, Task: taskErr}
	queue <- Job{ID: 2, Task: taskErr}
	queue <- Job{ID: 2, Task: taskErr}
	queue <- Job{ID: 2, Task: taskErr}
	queue <- Job{ID: 2, Task: taskErr}
	queue <- Job{ID: 2, Task: taskErr}

	time.Sleep(100 * time.Millisecond)
	queue <- Job{ID: 1, Task: taskTwo}

	time.Sleep(100 * time.Millisecond)

	for i := 0; i < 10; i++ {
		fmt.Printf("Mendaftarkan antrian #%d\n", i+3)
		queue <- Job{ID: i + 3, Task: taskOne}
		time.Sleep(100 * time.Millisecond)
	}

	// Menutup channel antrian, tetapi woker masih bekerja
	close(queue)
	fmt.Println("Antrian sudah ditutup. Siap-siap pulang!\n")

	wg.Wait()
	fmt.Println("Semua worker sudah selesai bekerja\n")

	fmt.Println("Selesai!")
}

func taskOne(ctx context.Context) error {
	time.Sleep(500 * time.Millisecond)
	return nil
}

func taskTwo(ctx context.Context) error {
	time.Sleep(500 * time.Millisecond)
	return nil
}

func taskErr(ctx context.Context) error {
	time.Sleep(10 * time.Millisecond)
	return fmt.Errorf("random error")
}
