package main

import (
	"context"
	"fmt"
	"sync"
	"time"
)

func worker(ctx context.Context, wg *sync.WaitGroup) {
	defer wg.Done()

	for {
		select {
		case <-ctx.Done():
			fmt.Println("Dibatalkan!")
			return
		default:
			fmt.Println("Masih bekerja...")
			time.Sleep(1 * time.Second)
		}
	}
}

func main() {
	parentCtx := context.Background()
	ctx, cancel := context.WithCancel(parentCtx)
	wg := &sync.WaitGroup{}

	wg.Add(1)
	go worker(ctx, wg)

	go func() {
		time.Sleep(5 * time.Second)
		cancel()
	}()

	wg.Wait()

	fmt.Println("Selesai")
}
