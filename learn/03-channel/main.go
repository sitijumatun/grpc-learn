package main

import (
	"fmt"
	"time"
)

func main() {
	// Unbuffered channel
	courier := make(chan string)

	// Receiver channel
	go func() {
		msg := <-courier
		fmt.Printf("Telah diterima sebuah pesan berisikan: %s\n", msg)
	}()

	fmt.Println("Aplikasi siap mengirim pesan melalui courier channel")

	// Sender
	courier <- "Halo mbak berkebaya merah"

	time.Sleep(5 * time.Second)

}
