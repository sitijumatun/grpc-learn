package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

func worker(queues chan string, spv *sync.WaitGroup) {
	defer spv.Done()

	for name := range queues {
		fmt.Printf("Sedang melayani nasabah atasnama: %s\n", name)

		time.Sleep(time.Second*3 + time.Duration(rand.Intn(16)))

		fmt.Printf("Selesai melayani nasabah atasnama: %s\n", name)
	}
}

func main() {
	// Buffered channel
	queues := make(chan string, 1)

	spv := &sync.WaitGroup{}

	for i := 0; i < cap(queues); i++ {
		spv.Add(1)
		go worker(queues, spv)
	}

	for j := 0; j < 10; j++ {
		fmt.Printf("Nasabah ke-%d masuk antrian\n", j)
		queues <- fmt.Sprintf("Nasabah ke-%d", j)
	}

	close(queues)

	spv.Wait()
}
