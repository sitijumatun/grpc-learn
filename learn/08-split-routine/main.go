package main

import (
	"fmt"
	"math"
	"sync"
	"time"
)

func main() {
	var data []int
	var start time.Time

	for i := 1; i <= 1000000; i++ {
		data = append(data, i)
	}

	// TANPA GO ROUTINES
	var hasil int

	start = time.Now()
	hasil = sum(data)

	fmt.Println("\n\nTANPA GO ROUTINES")
	fmt.Printf("Menghabiskan waktu : %d\n", time.Since(start)/1000)
	fmt.Printf("Hasil penjumlahan : %d\n", hasil)

	// DENGAN GO ROUTINES
	var hasil1 int
	var hasil2 int
	var hasil3 int

	start = time.Now()

	jmlWorker := 3
	bebanWorker := int(math.Floor(float64(len(data) / jmlWorker)))

	var wg sync.WaitGroup

	wg.Add(jmlWorker)

	offset1 := 0
	offset2 := offset1 + bebanWorker
	offset3 := offset2 + bebanWorker

	// Worker 1
	go func() {
		defer wg.Done()
		hasil1 = sum(data[offset1:bebanWorker])
	}()

	// Worker 2
	go func() {
		defer wg.Done()
		hasil2 = sum(data[offset2:offset2 + bebanWorker])
	}()

	// Worker 3
	go func() {
		defer wg.Done()
		hasil3 = sum(data[offset3:])
	}()

	wg.Wait()

	totalHasil := hasil1 + hasil2 + hasil3

	fmt.Println("\n\nDENGAN GO ROUTINES")
	fmt.Printf("Menghabiskan waktu : %d\n", time.Since(start)/1000)

	fmt.Printf("Worker 1 menjumlahkan : %d\n", hasil1)
	fmt.Printf("Worker 2 menjumlahkan : %d\n", hasil2)
	fmt.Printf("Worker 3 menjumlahkan : %d\n", hasil3)
	fmt.Printf("Hasil penjumlahan : %d\n", totalHasil)
}

func sum(data []int) int {
	var hasil int

	for _, num := range data {
		hasil = reduce(hasil, num)
	}

	return hasil
}

func reduce(carry int, num int) int {
	return carry + num
}
