package undo

type Stack struct {
	states   []State
	capacity int
}

func NewStack(capacity int) *Stack {
	s := make([]State, 0, 1)
	stack := &Stack{
		states:   s,
		capacity: capacity,
	}
	return stack
}

func (s *Stack) Pop() State {
	n := len(s.states)
	if n == 0 {
		return nil
	}
	v := s.states[n-1]
	s.states = s.states[:n-1]
	return v
}

func (s *Stack) Push(elem State) {
	n := len(s.states)
	if s.capacity > 0 && n == s.capacity {
		s.states = s.states[1:n]
	}
	s.states = append(s.states, elem)
}

func (s *Stack) Len() int {
	return len(s.states)
}

func (s *Stack) Clear() {
	s.states = make([]State, 0, 1)
}
