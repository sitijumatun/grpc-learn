package undo

type State interface{}

type Undoer interface {
	State() State
	Save(newState State)
	Undo()
	Redo()
	Clear()
}

type History struct {
	undos   *Stack
	current State
	redos   *Stack
	limit   int
}

func NewUndoer(limit int) Undoer {
	undos := NewStack(limit)
	redos := NewStack(limit)
	h := &History{
		undos:   undos,
		current: nil,
		redos:   redos,
		limit:   limit,
	}
	return h
}

func (h *History) State() State {
	return h.current
}

func (h *History) Save(newState State) {
	h.undos.Push(h.current)
	h.current = newState
	h.redos.Clear()
}

func (h *History) Undo() {
	state := h.undos.Pop()
	if state == nil {
		return
	}
	h.redos.Push(h.current)
	h.current = state
}

func (h *History) Redo() {
	state := h.redos.Pop()
	if state == nil {
		return
	}
	h.undos.Push(h.current)
	h.current = state
}

func (h *History) Clear() {
	h.undos.Clear()
	h.redos.Clear()
}
