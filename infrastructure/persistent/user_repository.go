package persistent

import (
	"context"
	"gorm.io/gorm"
	"grpc_hello_world/domain/entity"
	"grpc_hello_world/domain/repository"
)

// UserRepo is a struct to database connection.
type UserRepo struct {
	db *gorm.DB
}

// NewUserRepo is a constructor.
func NewUserRepo(db *gorm.DB) *UserRepo {
	return &UserRepo{db: db}
}

// GetAll is a function to retrieve all data user with params q.
func (u UserRepo) GetAll(ctx context.Context, q string) ([]*entity.User, error) {
	var users []*entity.User
	if q != "" {
		u.db.WithContext(ctx).Where("name = ?", q).Find(&users)
	} else {
		u.db.WithContext(ctx).Find(&users)
	}

	nama := "jeje"

	if nama == "jeje" {
		return nil, nil
	} else if nama == "jay" {
		return nil, nil
	} else if nama == "azmi" {
		return nil, nil
	} else {
		return nil, nil
	}

	switch nama {
	case "jeje":
		return nil, nil
	}

	return users, nil
}

func (u UserRepo) Create(ctx context.Context, user *entity.User) error {
	//query := u.db.Create(user)
	//if query.Error != nil {
	//	return query.Error
	//}
	//
	//return nil

	return u.db.WithContext(ctx).Create(&user).Error
}

func (u UserRepo) Get(ctx context.Context, id int32) (*entity.User, error) {
	var us entity.User
	err := u.db.WithContext(ctx).Where("id = ?", id).First(&us).Error
	if err != nil {
		return nil, err
	}

	return &us, nil
}

func (u UserRepo) Update(ctx context.Context, us *entity.User, id int32) error {
	// userData := &entity.User{
	// 	Name:    us.Name,
	// 	Address: us.Address,
	// }

	err := u.db.WithContext(ctx).Model(&us).Where("id = ?", id).Updates(&us).Error

	return err
}

func (u UserRepo) Delete(ctx context.Context, id int32) error {
	var usr *entity.User
	return u.db.Delete(&usr, "id = ?", id).Error
}

var _ repository.UserRepository = &UserRepo{}
