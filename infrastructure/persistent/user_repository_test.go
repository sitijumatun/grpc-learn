package persistent_test

import (
	"context"
	"fmt"
	"github.com/joho/godotenv"
	"github.com/stretchr/testify/assert"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"grpc_hello_world/config"
	"grpc_hello_world/domain/entity"
	"grpc_hello_world/infrastructure/persistent"
	"log"
	"testing"
)

func TestUserRepo_GetAll(t *testing.T) {
	err := godotenv.Load("../../.env")
	if err != nil {
		log.Println("no .env file provided")
	}

	// Init Config
	conf := config.New()
	dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s sslmode=disable TimeZone=Asia/Jakarta",
		conf.TestDBConfig.DBHost,
		conf.TestDBConfig.DBUser,
		conf.TestDBConfig.DBPassword,
		conf.TestDBConfig.DBName,
		conf.TestDBConfig.DBPort,
	)

	// Prepare database connection
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})
	db.Exec("DROP TABLE IF EXISTS users")

	db.AutoMigrate(&entity.User{})

	// Insert dummy data
	db.Create(&entity.User{
		ID:      1,
		Name:    "jeje",
		Address: "jogja",
	})
	db.Create(&entity.User{
		ID:      2,
		Name:    "jay",
		Address: "malang",
	})

	ctx := context.Background()

	t.Run("if success get all users with q jeje", func(t *testing.T) {
		userRepo := persistent.NewUserRepo(db)
		users, err := userRepo.GetAll(ctx, "jeje")

		assert.NoError(t, err)
		assert.Len(t, users, 1)
	})

	t.Run("if empty search with q azmi", func(t *testing.T) {
		userRepo := persistent.NewUserRepo(db)
		users, err := userRepo.GetAll(ctx, "azmi")

		assert.NoError(t, err)
		assert.Len(t, users, 0)
	})

	t.Run("if user get by id 1", func(t *testing.T) {
		userRepo := persistent.NewUserRepo(db)
		user, err := userRepo.Get(ctx, 1)

		assert.NoError(t, err)
		assert.NotNil(t, user)
	})

	t.Run("if success create user", func(t *testing.T) {
		userRepo := persistent.NewUserRepo(db)
		userDataProcess := &entity.User{
			ID:      3,
			Name:    "Ini Coba Nama",
			Address: "Ini Coba Address",
		}
		err := userRepo.Create(ctx, userDataProcess)
		userExist, err := userRepo.Get(ctx, 3)

		assert.NoError(t, err)
		assert.NotNil(t, userExist)
	})

	t.Run("if error create user", func(t *testing.T) {
		userRepo := persistent.NewUserRepo(db)
		userDataProcess := &entity.User{
			ID:      2,
			Name:    "Ini Coba Nama",
			Address: "Ini Coba Address",
		}
		err := userRepo.Create(ctx, userDataProcess)
		assert.Error(t, err)
	})

	t.Run("user-delete", func(t *testing.T) {
		userRepo := persistent.NewUserRepo(db)
		err := userRepo.Delete(ctx, 2)
		assert.NoError(t, err)
	})

	t.Run("USER UPDATE", func(t *testing.T) {
		userRepo := persistent.NewUserRepo(db)

		userData := entity.User{
			Name:    "Azmiii",
			Address: "Sky",
		}

		err := userRepo.Update(ctx, &userData, 1)
		assert.Nil(t, err, "Error while Update")

		user, err := userRepo.Get(ctx, 1)
		assert.Nil(t, err, "Error while Get")

		assert.Equal(t, user.Name, userData.Name)
		assert.Equal(t, user.Address, userData.Address)
	})
}
