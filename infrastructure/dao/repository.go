package dao

import (
	"gorm.io/gorm"
	"grpc_hello_world/domain/repository"
	"grpc_hello_world/infrastructure/persistent"
)

type Repositories struct {
	DB       *gorm.DB
	UserRepo repository.UserRepository
	// BookRepo repository.BookRepository
}

func NewRepositories(db *gorm.DB) *Repositories {
	return &Repositories{
		DB:       db,
		UserRepo: persistent.NewUserRepo(db),
		// BookRepo: persistent.NewBookRepo(db),
	}
}
